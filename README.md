# README #

This repository is mainly for my own use. It's also for the benefit of people wanting to know what I've done to get my awesome UV hat to work.

### Needed Hardware ###

* An Adafruit Trinket or Flora (recommended)
* An Adafruit Si1145 Light Sensor - Flora or otherwise
* An Adafruit RGB Neopixel - Flora or otherwise
* A Piezo buzzer
* wire, conductive thread
* a hat (quite obviously)
* a Lipo/Li-ion battery for actually taking your project outdoors