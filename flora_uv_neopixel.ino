/***************************************************
 * Sunscreen reminder hat using the Si1145 UV/IR/Visible Light Sensor
 * visit https://learn.adafruit.com/sunscreen-reminder-hat for full tutorial
 * contained Super Mario Bros theme composed for piezo by Tiago Gala: http://therandombit.wordpress.com/2011/11/21/arduino-piezo-speaker-super-mario/
 * Originally written by Becky Stern for Adafruit Industries.
 * BSD license, all text above must be included in any redistribution
 
 ****************************************************/
 #include <Wire.h>
 #include "Adafruit_SI1145.h"
 #include "Adafruit_NeoPixel.h"

//tones for reminder tune
#define NOTE_FS4 370
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_B4  494
#define NOTE_CS5 554

int speaker = 1; // piezo wired to FLORA TX
int w = 1000;

Adafruit_SI1145 uv = Adafruit_SI1145();

uint32_t sinceTime;
uint32_t markerTime;
uint8_t loopNumber = 0;
uint8_t checkupLoop = 0;
boolean shouldChime = false;
float UVindex;
uint8_t UVrounded;
float UVthreshold = 2.0;

uint32_t notificationInterval = 900000; // 15 minutes = 900000 milliseconds. One hour = 3600000 milliseconds. Two hours = 7200000 milliseconds

Adafruit_NeoPixel led = Adafruit_NeoPixel(1, 6, NEO_GRB + NEO_KHZ800);

void setup() {
  Serial.begin(9600);
  Serial.println("Adafruit SI1145 test");
  if (! uv.begin()) {
    Serial.println("Didn't find Si1145");
    while (1);
  }
  Serial.println("OK!");
  pinMode(speaker, OUTPUT);
  led.begin();
  led.show(); // Initialize all pixels to 'off'
}

void loop() {
  Serial.println("===================");
  get_UV_number();
  Serial.print("UV: "); Serial.println(UVindex);
  loopNumber++;

  if (UVindex > UVthreshold) { //only chime when we are currently outside
    Serial.println("Play pips");
    pips(UVrounded);
    Serial.println("Show colour");
    ledColour(UVindex, UVrounded);
    Serial.print("Iteration number ");Serial.println(loopNumber);
    if (loopNumber >= 8) { //check to see if we've exceeded the time limit
      Serial.println("Playing music");
      chime();
      loopNumber = 0;
    }
    delay(notificationInterval);
  } else {
    loopNumber = 0;
    Serial.println("still alive!");
    delay(2000);
    checkupLoop++;
    Serial.println(checkupLoop);
    if (checkupLoop>=150){
      Serial.println("Breathe!");
      colorWipe(led.Color(2, 10, 18), 500, 1);
      checkupLoop = 0;
    }
  }
}

uint32_t get_UV_number() {
  UVindex = uv.readUV();
  UVindex /= 100.0; // the index is multiplied by 100 so to get the integer index, divide by 100!
  UVrounded = ceil(UVindex);
  return UVrounded;
}

void pips(uint8_t repeat){
  for (int pip = 0; pip < repeat; pip++){
    Serial.print("This is a pip! ");
    tone(speaker,NOTE_CS5,300);
    delay(500);
  }
  Serial.println();
}

void chime(){
  // notes in the melody:
  // Beatles - Here Comes the Sun - I think this is more appropriate than the Mario tune :-D
  int melody[] = {
    NOTE_CS5, NOTE_A4, NOTE_B4, NOTE_CS5,0, NOTE_A4 ,0, NOTE_CS5, NOTE_B4, NOTE_A4,NOTE_FS4, NOTE_A4, NOTE_B4,NOTE_A4, NOTE_FS4,NOTE_GS4,NOTE_FS4,NOTE_GS4,NOTE_A4,NOTE_B4,0,
    NOTE_CS5, NOTE_A4, NOTE_B4, NOTE_CS5,0, NOTE_A4 ,0, NOTE_CS5, NOTE_B4, NOTE_A4,NOTE_FS4,0,NOTE_CS5,NOTE_B4,NOTE_A4,NOTE_GS4,                          };

  // note durations: 4 = quarter note, 8 = eighth note, etc.:
  int noteDurations[] = {4,4,4,4,4,2,4,4,2,2,2,2,2,2,4,4,4,4,2,2,2,4,4,4,4,4,2,4,4,2,2,2,4,4,2,2,1,};
  Serial.println();
  Serial.println("This is the chime");
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 37; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000/noteDurations[thisNote];
    tone(speaker, melody[thisNote],noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.0;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(speaker);
  }
}

void ledColour(float UVindex, uint8_t repeat){
  if (UVindex < 3)
  {
    Serial.println("GREEN");
    colorWipe(led.Color(40, 149, 0), w, repeat);
  }
  else if (UVindex < 6)
  {
    Serial.println("YELLOW");
    colorWipe(led.Color(247, 228, 0), w, repeat);
  }
  else if (UVindex < 8)
  {
    Serial.println("ORANGE");
    colorWipe(led.Color(248, 89, 0), w, repeat);
  }
  else if (UVindex < 11)
  {
    Serial.println("RED");
    colorWipe(led.Color(255, 0, 0), w, repeat);
  }
  else if (UVindex >= 11)
  {
    Serial.println("PURPLE");
    colorWipe(led.Color(150, 0, 200), w, repeat);
  }
  else
  {
    Serial.println("ERROR ERROR ERROR");
  }
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait, uint8_t repeat) {
  for(uint16_t i=0; i<repeat; i++) {
    led.setPixelColor(0, c);
    led.show();
    delay(wait);
    led.setPixelColor(0, 0,0,0);
    led.show();
    delay(wait);
  }
}
